/**
* CIAA Tick Module
*
* Define CIAA properties for MPS communication in a Component Base Architecture
* using Message Passing Tick based communication model
*/
module ciaa


open Architecture/cbsemetamodel
open Architecture/messaging
open Architecture/connectorMPS
open Architecture/constraint
open Architecture/property
open util/ordering[messaging/Tick] as tick


/** Properties */

/// paylaodConfidentiality 
// if a component c intercept a message m and c is not allowed to get d
// then c is not able to get the payload d
sig PayloadConfidentiality extends ConnectorProperty  {}{
	let msg = { m:MsgPassing | m.payload = payl } | payloadConfidentiality[comp1,comp2,msg]
} 
pred payloadConfidentiality[s,r:Component, m:MsgPassing] { 
	all t:Tick, d:Payload | E_get_pld[r,m,d,t] and sent_by[m,s,t]  => sent_to[m,r,t] 
}

// payloadIntegrity : 
// if c is able to get the payload d of m
// then d is the authentic (accurate) payload of m
sig PayloadIntegrity extends ConnectorProperty  {}{
	let msg = { m:MsgPassing | m.payload = payl } | payloadIntegrity[comp1,comp2,msg]
} 
pred payloadIntegrity[s,r:Component, m:MsgPassing]  {
	all t:Tick, d:Payload | E_get_pld[r,m,d,t] and sent_by[m,s,t] => sent_with[m,d,t] 
}

// messageAvailibility : 
// if c1 inject a message m with c2 as intented receiver
// then c2 will finally be able to intercept the message
sig MessageAvailability extends ConnectorProperty  {}{
	let msg = { m:MsgPassing | m.payload = payl } | messageAvailability[comp1,comp2,msg]
} 
pred messageAvailability [s,r:Component, m:MsgPassing] {
	all t1:Tick |  some t2:t1.nexts |  inject[s,m,t1] and m.receiver = r => E_intercept[r,m,t2]
}

// messageAuthenticity : 
// if c is able to get the sender s of m
// then s is the authentic (accurate) origin of m
sig MessageAuthenticity extends ConnectorProperty  {}{
	let msg = { m:MsgPassing | m.payload = payl } | messageAuthenticity[comp1,comp2,msg]
} 
pred messageAuthenticity[s,r:Component, m:MsgPassing]  {
	all t:Tick | E_get_src[r,m,s,t] implies sent_by[m,s,t] 
}


/**
Policies
*/
// Defines the confidentiality policy for a MsgPassing m
sig AllowedGetPld {
	msg: one MsgPassing,
	comp: set Component
}
fact {
	no disj al1,al2:AllowedGetPld | some al1.msg & al2.msg
}

pred restrictiveGetPld[m:MsgPassing] {
	all c:Component, t:Tick |
		E_inject[c,m,t] => some al:AllowedGetPld |  al.msg = m and  al.comp = m.receiver
	all c:Component, t:Tick, d:Payload |
		E_get_pld[c,m,d, t] => some al:AllowedGetPld | al.msg = m and c =	 al.comp
}

// Defines the integrity policy for a MsgPassing m
sig AllowedSetPld {
	msg: one MsgPassing,
	comp: one Component
}
fact {
	no disj al1,al2:AllowedSetPld | some al1.msg & al2.msg
}

pred restrictiveSetPld[m:MsgPassing] { 
		all c:Component, t:Tick | 
			E_inject[c,m,t] => some al:AllowedSetPld | al.msg = m and al.comp = c
		all c:Component, t:Tick, d:Payload | 
			E_set_pld[c,m,d, t] => some al:AllowedSetPld | al.msg = m and al.comp = c
}

// Defines the availibility policy for a MsgPassing m
sig AllowedIntercept {
	msg: one MsgPassing,
	comp: set Component
}
fact {
	no disj al1,al2:AllowedIntercept | some al1.msg & al2.msg
}

pred lossFreeInject[m:MsgPassing] {
		 // Once a message is injected it remains in c.buffer unless it is intercepted by the intended receiver
		all c1:Component, t1:Tick | let c2=m.receiver |
			inject[c1,m,t1] =>  
				some al:AllowedIntercept, con:Connector | all t2:t1.nexts |
					m not in con.buffer.t2 iff al.msg = m and al.comp = c2  and intercept[c2,m,t2] 
}

// Defines the authenticity policy for a MsgPassing m
sig AllowedSetSrc {
	msg: one MsgPassing,
	comp: one Component
}
fact {
	no disj al1,al2:AllowedSetSrc | some al1.msg & al2.msg
}

pred restrictiveSetSrc[m:MsgPassing] {
		all c,s:Component, t:Tick | 
			E_set_src[c,m,s,t] => some al:AllowedSetSrc | al.msg = m and al.comp = s
		all c:Component, t:Tick | 
			E_inject[c,m,t] => some al:AllowedSetSrc | al.msg= m and al.comp = c
}



// conf: A RestrictiveGetPld ConnectorMPS offers that all messages transmited by respect the RestrictiveGetPld policy 
pred ConnectorMPS.RestrictiveGetPld {
	all m:MsgPassing, t:Tick |
		m in this.buffer.t => restrictiveGetPld[m]
}

// intg: A RestrictiveSetPld ConnectorMPS offers that all messages transmited by respect the RestrictiveSetPld policy
pred ConnectorMPS.RestrictiveSetPld {
	all m:MsgPassing, t:Tick |
		m in this.buffer.t => restrictiveSetPld[m]
}

// avl: A LossFreeInject ConnectorMPS offers that all messages transmited by respect the LossFreeInject policy
pred ConnectorMPS.LossFreeInject {
	all m:MsgPassing, t:Tick |
		m in this.buffer.t => lossFreeInject[m]
}

// auth: A RestrictiveSetSrc ConnectorMPS offers that all messages transmited by respect the RestrictiveSetSrc policy
pred ConnectorMPS.RestrictiveSetSrc {
	all m:MsgPassing, t:Tick |
		m in this.buffer.t => restrictiveSetSrc[m]	
}


/** Verification */
// Confidentiality & integrity & authenticity & availability must not hold on base system 
assert confidentialityNotHold {
	all c1,c2: Component, m:MsgPassing |  payloadConfidentiality[c1, c2, m]
}

assert integrityNotHold {
	all c1,c2: Component, m:MsgPassing |   payloadIntegrity[c1, c2, m] 
}

assert authencityNotHold {
	all c1,c2: Component, m:MsgPassing |   messageAuthenticity[c1, c2, m]   
}

assert availabilityNotHold {
	all c1,c2: Component, m:MsgPassing |  	messageAvailability[c1,c2, m] 
}

check confidentialityNotHold for 5 // CounterExample must be found
check integrityNotHold for 5 // CounterExample must be found
check authencityNotHold for 4 // CounterExample must be found
check availabilityNotHold for 4 // CounterExample must be found

// confidentiality must hold with connectors enforcing restrictiveGetPld policy
assert confidentialityHold {
	 (all c:ConnectorMPS | c.RestrictiveGetPld ) => all c1,c2: Component, m:MsgPassing | payloadConfidentiality[c1, c2, m]
}

// integrity must hold with connectors enforcing restrictiveSetPld policy
assert integrityHold  {
	 (all c:Connector | c.RestrictiveSetPld) => all c1,c2: Component, m:MsgPassing |  payloadIntegrity[c1, c2, m] 
}

// availability must hold with connectors enforcing lossFreeInject policy
assert availabilityHold {
	(all c:Connector | c.LossFreeInject )  => all c1,c2: Component, m:MsgPassing | messageAvailability[c1, c2, m]    
}

// authencity must hold with connectors enforcing restrictiveSetSrc policy
assert authencityHold {
	(all c:Connector | c.RestrictiveSetSrc )  => all c1,c2: Component, m:MsgPassing |  messageAuthenticity[c1,c2, m] 
}

check confidentialityHold for 5 // No CounterExample must be found
check integrityHold for 5 // No CounterExample must be found
check availabilityHold for 5 // No CounterExample must be found
check authencityHold  for 5 // No CounterExample must be found




